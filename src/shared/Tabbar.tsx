import React  from 'react';

interface TabBarProps {
  onChangeSection: (idx: number) => void
}
export const TabBar: React.FC<TabBarProps> = ({ onChangeSection }) => {
  return <div>
    <button onClick={() => onChangeSection(0)}>Demo1</button>
    <button onClick={() => onChangeSection(1)}>Demo2</button>
  </div>
};
