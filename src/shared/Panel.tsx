import React, { useState } from 'react';
import cn from 'classnames';
import css from './Panel.module.css';

interface PanelProps {
  title: string;
  color?: string;
}

export const Panel: React.FC<PanelProps> = ({
  title,
  children,
  color = 'black'
}) => {
  const [isOpen, setIsOpen] = useState<boolean>(true);

  console.log('render: Panel')
  return (
    <div className={cn('card', css.title)}>

      {/*Title*/}
      <div
        className="card-header"
        style={{ color }}
        onClick={() => setIsOpen(!isOpen)}
      >{title}</div>

      {/*Body*/}
      <div
        className={cn('card-body', {'d-none': !isOpen})}
      > {children} </div>

    </div>
  )

}
