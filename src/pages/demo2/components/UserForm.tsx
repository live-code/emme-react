import React, { useState } from 'react';
import cn from 'classnames';
import { User } from '../../../model/user';

interface UserFormsProps {
  onSave: (data: Pick<User, 'name' | 'email'>) => void
}
export const UserForm: React.FC<UserFormsProps> = (props) => {
  const [formData, setFormData] = useState<Pick<User, 'name' | 'email'>>({ name: '', email: ''})


  function onChangeHandler(event: React.ChangeEvent<HTMLInputElement>) {
    setFormData({
      ...formData,
      [event.currentTarget.name]: event.currentTarget.value,
    })
  }

  function saveHandler(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    props.onSave(formData)
  }

  const isNameValid = formData.name.length !== 0;
  const isEmailValid =  formData.email.length !== 0;
  const isValid = isNameValid && isEmailValid;

  return (
      <form onSubmit={saveHandler}>
        <input
          className={cn('form-control', {'is-invalid': !isNameValid, 'is-valid': isNameValid})}
          type="text"
          value={formData.name}
          placeholder="name"
          onChange={onChangeHandler}
          name="name"
        />
        <input
          className={cn('form-control', {'is-invalid': !isEmailValid})}
          type="text" value={formData.email} placeholder="email" onChange={onChangeHandler} name="email"/>
        <button disabled={!isValid} type="submit">SAVE</button>
      </form>
  )
};
