import React  from 'react';
import { Link } from 'react-router-dom';
import { User } from '../../../model/user';

interface UserListProps {
  data: User[];
  onDeleteUser: (id: number) => void
}
export const UserList: React.FC<UserListProps> = (props) => {
  return <div>
    {/* List */}
    {
      props.data.map(user => {
        return <li key={user.id}>
          {user.name}
          <i className="fa fa-trash"
             onClick={() => props.onDeleteUser(user.id)}
          />

          <Link to={'/demo2-details/' + user.id}>
            <i className="fa fa-info-circle"
               onClick={() => props.onDeleteUser(user.id)}
            />
          </Link>
        </li>
      })
    }
  </div>
};
