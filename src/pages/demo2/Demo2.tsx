import React from 'react';
import { User } from '../../model/user';
import { UserList } from './components/UserList';
import { UserForm } from './components/UserForm';
import { useUsers } from './hooks/useUsers';

export const Demo2: React.FC = () => {
  const { users, error, actions } = useUsers();
  return (
    <div>
      {error && <div className="alert alert-danger">Errore</div>}
      <Message users={users} />
      <UserForm onSave={actions.saveUser} />
      <UserList data={users} onDeleteUser={actions.deleteUser}/>
    </div>
  )
};


// ----
export interface MessageProps {
  users: User[]
}
export const Message: React.VFC<MessageProps> = (props) => {
  return <div>Ci sono {props.users.length} users</div>
}
