import { useEffect, useState } from 'react';
import { User } from '../../../model/user';
import axios from 'axios';

export function useUsers() {
  const [users, setUsers] = useState<User[]>([]);
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    console.log('useEffect')
    axios.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .then(res => setUsers(res.data))
      .catch(err => setError(true))

    return () => {
      console.log('destroy')
    }
  }, [])

  function deleteHandler(id: number) {
    setError(false)
    axios.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then(() => setUsers(users.filter(u => u.id !== id)))
      .catch(err => setError(true))
  }

  function saveHandler(data: Pick<User, 'name' | 'email'>) {
    axios.post<User>(`https://jsonplaceholder.typicode.com/users/`, data)
      .then(res => setUsers([...users, res.data]))
  }

  return {
    users,
    error,
    actions: {
      deleteUser: deleteHandler,
      saveUser: saveHandler
    }
  }
}
