import React, { useState } from 'react';
import { Panel } from '../../shared/Panel';

export function Demo1() {
  const [counter, setCounter] = useState(10)

  console.log('render:App');

  return (
    <div className="container mt-3">
      {counter}
      <button onClick={() => setCounter(counter + 1)}>+</button>

      <Panel title={'title'} color="red">
        <input type="text"/>
        <input type="text"/>
      </Panel>

    </div>
  );
}



