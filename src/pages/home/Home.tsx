import React, { useContext, useEffect, useState } from 'react';

interface HomeState {
  count: number;
  text?: string;
}

const HomeContext = React.createContext<HomeState | null>(null)

const Home: React.FC = () => {
  const [data, setData] = useState<HomeState>({ count: 0, text: 'pippo'});
  console.log('home')
  return <HomeContext.Provider value={data}>
    <h1>Home</h1>
    <HomeDashboard />

    <button onClick={() => setData(state => ({ ...state, count: state.count + 1}))}>+</button>
    <button onClick={() => setData({ ...data, count: data.count + 1})}>+</button>
    <button onClick={() => setData({ ...data, text: 'ciccio'})}>ciccio</button>
    <button onClick={() => setData({ ...data, text: 'pippo'})}>pippo</button>
  </HomeContext.Provider>
};
export default Home;

// ....

const HomeDashboard: React.FC = React.memo(() => {
  console.log(' HomeDashboard')
  return <div>
    <h2>Dashboard</h2>
    <Widget />
  </div>
})


const Widget: React.FC = () => {
  console.log('  widget')
  const state = useContext(HomeContext);

  <h1>Widget</h1>
  return <div>Widget: { state?.count} </div>
}



// ....
