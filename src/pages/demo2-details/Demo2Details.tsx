import React, { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import axios from 'axios';
import { User } from '../../model/user';

type TParams = { id: string };

export const Demo2Details: React.FC<RouteComponentProps<TParams>> = (props) => {
  const [user, setUser] = useState<User | null>(null);
  const [error, setError] = useState<boolean>(false)

  useEffect(() => {
    const id = props.match.params.id;

    axios.get<User>('https://jsonplaceholder.typicode.com/users/' + id)
      .then(res => setUser(res.data))
      .catch(() => setError(true))
  }, [props.match.params.id])

  return <div>
    {error && <div className="alert">User not found</div>}
    {user && <h1>{user.name}</h1>}
    {user?.email}

  </div>
};
