import React, { useState, lazy, Suspense } from 'react';
import { Demo1 } from './pages/demo1/Demo1';
import { Demo2 } from './pages/demo2/Demo2';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { NavBar } from './core/NavBar';
import { Login } from './pages/login/Login';
import { Demo2Details } from './pages/demo2-details/Demo2Details';
const Home = lazy(() => import('./pages/home/Home'))

function App() {
  return (
    <BrowserRouter>
      <NavBar />

      <div className="container mt-3">
        <Suspense fallback={<div>loading....</div>}>
          <hr/>
          <Switch>
            <Route path="/home">
              <Home />
            </Route>

            <Route path="/login">
              <Login />
            </Route>

            <Route path="/demo1">
              <Demo1 />
            </Route>

            <Route path="/demo2" component={Demo2} />
            <Route path="/demo2-details/:id" component={Demo2Details} />

            <Route path="*">
              <Redirect to="login" />
            </Route>
          </Switch>
        </Suspense>
      </div>
    </BrowserRouter>
  );
}

export default App;



