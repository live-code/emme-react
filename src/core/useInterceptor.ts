// INTERCEPTOR EXAMPLE

import { useEffect, useState } from 'react';
import Axios from 'axios';
// import { getItemFromLocalStorage } from './localstorage.helper';

// USAGE
// const {  error, request } = useInterceptor();

export const useInterceptor = () => {
  const [request, setRequest] = useState<any>(null)
  const [error, setError] = useState<boolean>(false)

  useEffect(() => {
    Axios.interceptors.request.use( (config) => {
      const req = {
        ...config,
        headers: {
          ...config.headers,
          // 'Authentication-JWT': 'bearer ' + getItemFromLocalStorage('token')
        }
      };

      setError(false);
      setRequest(req)

      return req;
    });

    Axios.interceptors.response.use( (response) => {
      return response;
    }, function (error) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      console.log('ERROR', error)
      setError(true);
      return Promise.reject(error);
    });

  },[])

  return {
    request,
    error
  };
}


/*
export const IfLogged: React.FC = (props) => {
  return isLogged() ? <>{props.children}</> :  null;
};
*/



/**
 import React from 'react';
 import { Redirect, Route } from 'react-router';
 import { isLogged } from './authentication.service';



 export const PrivateRoute: React.FC<RouteProps> = ({ children, ...rest }) => {
  return (
    <Route {...rest}>
      {
        isLogged() ?
          children :
          <Redirect to={{ pathname: "/login" }} />
      }
    </Route>
  );
};
*/


/*
<PrivateRoute to="/home" role="admin">
  <Home>
</Private>*/
