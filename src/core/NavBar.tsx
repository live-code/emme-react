import React from 'react';
import { NavLink } from 'react-router-dom';

export const NavBar: React.FC = () => {

  return (
    <nav className="navbar navbar-expand navbar-light bg-light">
      <div className="navbar-brand">
        <NavLink to="/login">React</NavLink>
      </div>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/login">Login</NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/home">Home</NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/demo1">Demo1</NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              exact={ true }
              className="nav-link"
              to="/demo2">Demo2</NavLink>

          </li>


        </ul>
      </div>
    </nav>
  )
};
